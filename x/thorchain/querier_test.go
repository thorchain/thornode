package thorchain

import (
	"encoding/json"
	"strconv"

	"github.com/blang/semver"

	. "gopkg.in/check.v1"

	"github.com/cosmos/cosmos-sdk/codec"
	codectypes "github.com/cosmos/cosmos-sdk/codec/types"
	cryptocodec "github.com/cosmos/cosmos-sdk/crypto/codec"
	"github.com/cosmos/cosmos-sdk/crypto/hd"
	ckeys "github.com/cosmos/cosmos-sdk/crypto/keyring"
	types2 "github.com/cosmos/cosmos-sdk/types"

	"gitlab.com/thorchain/thornode/v3/cmd"
	"gitlab.com/thorchain/thornode/v3/common"
	"gitlab.com/thorchain/thornode/v3/common/cosmos"
	openapi "gitlab.com/thorchain/thornode/v3/openapi/gen"
	"gitlab.com/thorchain/thornode/v3/x/thorchain/keeper"
	"gitlab.com/thorchain/thornode/v3/x/thorchain/types"
)

// Note: coverage lacking for these queries in this file
//   check whether there is coverage elsewhere to add openapi conformance verification
// QueryBlockResponse
// QueryBorrowerResponse
// QueryBorrowersResponse
// QueryDerivedPoolResponse
// QueryDerivedPoolsResponse
// QueryLiquidityProviderResponse
// QueryMimirAdminValuesResponse
// QueryMimirNodesAllValuesResponse
// QueryMimirNodesValuesResponse
// QueryMimirWithKeyResponse
// QueryOutboundFeesResponse
// QueryOutboundResponse
// QueryPoolSlipsResponse
// QueryQuoteLoanCloseResponse
// QueryQuoteLoanOpenResponse
// QueryQuoteSaverDepositResponse
// QueryQuoteSaverWithdrawResponse
// QueryQuoteSwapResponse
// QueryRuneProviderResponse
// QueryRuneProvidersResponse
// QuerySaverResponse
// QueryStreamingSwapResponse
// QueryStreamingSwapsResponse
// SwapperClout
// QuerySwapQueueResponse
// QueryThornameResponse
// QueryTradeAccountsResponse
// QueryTradeUnitsResponse
// QueryTssKeygenMetricResponse
// QueryTssMetricResponse
// QueryInvariantResponse
// QueryInvariantsResponse
// QueryRunePoolResponse
// QueryTradeUnitResponse

type QuerierSuite struct {
	kb          cosmos.KeybaseStore
	mgr         *Mgrs
	k           keeper.Keeper
	queryServer types.QueryServer
	ctx         cosmos.Context
}

var _ = Suite(&QuerierSuite{})

type TestQuerierKeeper struct {
	keeper.KVStoreDummy
	txOut *TxOut
}

func (k *TestQuerierKeeper) GetTxOut(_ cosmos.Context, _ int64) (*TxOut, error) {
	return k.txOut, nil
}

func (s *QuerierSuite) SetUpTest(c *C) {
	registry := codectypes.NewInterfaceRegistry()
	cryptocodec.RegisterInterfaces(registry)
	cdc := codec.NewProtoCodec(registry)
	kb := ckeys.NewInMemory(cdc)
	username := "thorchain"
	password := "password"

	_, _, err := kb.NewMnemonic(username, ckeys.English, cmd.THORChainHDPath, password, hd.Secp256k1)
	c.Assert(err, IsNil)
	s.kb = cosmos.KeybaseStore{
		SignerName:   username,
		SignerPasswd: password,
		Keybase:      kb,
	}
	s.ctx, s.mgr = setupManagerForTest(c)
	s.k = s.mgr.Keeper()
	s.queryServer = NewQueryServerImpl(s.mgr, s.kb)
}

func (s *QuerierSuite) TestQueryKeysign(c *C) {
	ctx, _ := setupKeeperForTest(c)
	ctx = ctx.WithBlockHeight(12)

	pk := GetRandomPubKey()
	toAddr := GetRandomETHAddress()
	txOut := NewTxOut(1)
	txOutItem := TxOutItem{
		Chain:       common.ETHChain,
		VaultPubKey: pk,
		ToAddress:   toAddr,
		InHash:      GetRandomTxHash(),
		Coin:        common.NewCoin(common.ETHAsset, cosmos.NewUint(100*common.One)),
	}
	txOut.TxArray = append(txOut.TxArray, txOutItem)
	keeper := &TestQuerierKeeper{
		txOut: txOut,
	}

	_, mgr := setupManagerForTest(c)
	mgr.K = keeper
	queryServer := NewQueryServerImpl(mgr, s.kb)

	queryKeysignResp, err := queryServer.KeysignPubkey(ctx, &types.QueryKeysignPubkeyRequest{
		Height: "5",
		PubKey: pk.String(),
	})
	c.Assert(err, IsNil)
	c.Assert(queryKeysignResp, NotNil)

	// Verify conformance to openapi spec
	result, err := queryKeysignResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	var openapiKeysignResp openapi.KeysignResponse
	err = json.Unmarshal(result, &openapiKeysignResp)
	c.Assert(err, IsNil)
	c.Assert(openapiKeysignResp.Signature, Equals, queryKeysignResp.Signature)
	c.Assert(*openapiKeysignResp.Keysign.Height, Equals, queryKeysignResp.Keysign.Height)
	c.Assert(len(openapiKeysignResp.Keysign.TxArray), Equals, len(queryKeysignResp.Keysign.TxArray))
}

func (s *QuerierSuite) TestQueryPool(c *C) {
	ctx, mgr := setupManagerForTest(c)
	queryServer := NewQueryServerImpl(mgr, s.kb)

	pubKey := GetRandomPubKey()
	asgard := NewVault(ctx.BlockHeight(), ActiveVault, AsgardVault, pubKey, common.Chains{common.ETHChain}.Strings(), []ChainContract{})
	c.Assert(mgr.Keeper().SetVault(ctx, asgard), IsNil)

	poolETH := NewPool()
	poolETH.Asset = common.ETHAsset
	poolETH.LPUnits = cosmos.NewUint(100)

	poolBTC := NewPool()
	poolBTC.Asset = common.BTCAsset
	poolBTC.LPUnits = cosmos.NewUint(0)

	err := mgr.Keeper().SetPool(ctx, poolETH)
	c.Assert(err, IsNil)

	err = mgr.Keeper().SetPool(ctx, poolBTC)
	c.Assert(err, IsNil)

	queryPoolsResp, err := queryServer.Pools(ctx, &types.QueryPoolsRequest{})
	c.Assert(err, IsNil)
	res, err := queryPoolsResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	var out Pools

	err = json.Unmarshal(res, &out)
	c.Assert(err, IsNil)
	c.Assert(len(out), Equals, 1)

	poolBTC.LPUnits = cosmos.NewUint(100)
	err = mgr.Keeper().SetPool(ctx, poolBTC)
	c.Assert(err, IsNil)

	queryPoolsResp, err = queryServer.Pools(ctx, &types.QueryPoolsRequest{})
	c.Assert(err, IsNil)
	res, err = queryPoolsResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	err = json.Unmarshal(res, &out)
	c.Assert(err, IsNil)
	c.Assert(len(out), Equals, 2)

	// Query pool with asset ETH.ETH from different context
	queryPoolResp, err := queryServer.Pool(s.ctx, &types.QueryPoolRequest{
		Asset: "ETH.ETH",
	})
	c.Assert(queryPoolResp, IsNil)
	c.Assert(err, NotNil)
}

func (s *QuerierSuite) TestVaults(c *C) {
	ctx, mgr := setupManagerForTest(c)
	queryServer := NewQueryServerImpl(mgr, s.kb)

	pubKey := GetRandomPubKey()
	asgard := NewVault(ctx.BlockHeight(), ActiveVault, AsgardVault, pubKey, common.Chains{common.ETHChain}.Strings(), nil)
	c.Assert(mgr.Keeper().SetVault(ctx, asgard), IsNil)

	poolETH := NewPool()
	poolETH.Asset = common.ETHAsset
	poolETH.LPUnits = cosmos.NewUint(100)

	poolBTC := NewPool()
	poolBTC.Asset = common.BTCAsset
	poolBTC.LPUnits = cosmos.NewUint(0)

	err := mgr.Keeper().SetPool(ctx, poolETH)
	c.Assert(err, IsNil)

	err = mgr.Keeper().SetPool(ctx, poolBTC)
	c.Assert(err, IsNil)

	queryPoolsResp, err := queryServer.Pools(ctx, &types.QueryPoolsRequest{})
	c.Assert(err, IsNil)
	res, err := queryPoolsResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	var out Pools
	err = json.Unmarshal(res, &out)
	c.Assert(err, IsNil)
	c.Assert(len(out), Equals, 1)

	poolBTC.LPUnits = cosmos.NewUint(100)
	err = mgr.Keeper().SetPool(ctx, poolBTC)
	c.Assert(err, IsNil)

	queryPoolsResp, err = queryServer.Pools(ctx, &types.QueryPoolsRequest{})
	c.Assert(err, IsNil)
	res, err = queryPoolsResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	err = json.Unmarshal(res, &out)
	c.Assert(err, IsNil)
	c.Assert(len(out), Equals, 2)

	// Query pool with asset ETH.ETH from different context
	queryPoolResp, err := queryServer.Pool(s.ctx, &types.QueryPoolRequest{
		Asset: "ETH.ETH",
	})
	c.Assert(queryPoolResp, IsNil)
	c.Assert(err, NotNil)
}

func (s *QuerierSuite) TestSaverPools(c *C) {
	ctx, mgr := setupManagerForTest(c)
	queryServer := NewQueryServerImpl(mgr, s.kb)

	poolDOGE := NewPool()
	poolDOGE.Asset = common.DOGEAsset.GetSyntheticAsset()
	poolDOGE.LPUnits = cosmos.NewUint(100)

	poolBTC := NewPool()
	poolBTC.Asset = common.BTCAsset
	poolBTC.LPUnits = cosmos.NewUint(1000)

	poolETH := NewPool()
	poolETH.Asset = common.ETHAsset.GetSyntheticAsset()
	poolETH.LPUnits = cosmos.NewUint(100)

	err := mgr.Keeper().SetPool(ctx, poolDOGE)
	c.Assert(err, IsNil)

	err = mgr.Keeper().SetPool(ctx, poolBTC)
	c.Assert(err, IsNil)

	err = mgr.Keeper().SetPool(ctx, poolETH)
	c.Assert(err, IsNil)

	queryPoolsResp, err := queryServer.Pools(ctx, &types.QueryPoolsRequest{})
	c.Assert(err, IsNil)
	res, err := queryPoolsResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	var out []openapi.Pool
	err = json.Unmarshal(res, &out)
	c.Assert(err, IsNil)
	c.Assert(len(out), Equals, 1)
}

func (s *QuerierSuite) TestQueryNodeAccounts(c *C) {
	ctx, keeper := setupKeeperForTest(c)

	_, mgr := setupManagerForTest(c)
	queryServer := NewQueryServerImpl(mgr, s.kb)

	nodeAccount := GetRandomValidatorNode(NodeActive)
	c.Assert(keeper.SetNodeAccount(ctx, nodeAccount), IsNil)
	vault := GetRandomVault()
	vault.Status = ActiveVault
	vault.BlockHeight = 1
	c.Assert(keeper.SetVault(ctx, vault), IsNil)
	queryNodesResp, err := queryServer.Nodes(ctx, &types.QueryNodesRequest{})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	res, err := queryNodesResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	var out types.NodeAccounts
	err1 := json.Unmarshal(res, &out)
	c.Assert(err1, IsNil)
	c.Assert(len(out), Equals, 1)

	nodeAccount2 := GetRandomValidatorNode(NodeActive)
	nodeAccount2.Bond = cosmos.NewUint(common.One * 3000)
	c.Assert(keeper.SetNodeAccount(ctx, nodeAccount2), IsNil)

	// Check Bond-weighted rewards estimation works
	var nodeAccountResp []openapi.Node

	// Add bond rewards + set min bond for bond-weighted system
	network, _ := keeper.GetNetwork(ctx)
	network.BondRewardRune = cosmos.NewUint(common.One * 1000)
	c.Assert(keeper.SetNetwork(ctx, network), IsNil)
	keeper.SetMimir(ctx, "MinimumBondInRune", common.One*1000)

	queryNodesResp, err = queryServer.Nodes(ctx, &types.QueryNodesRequest{})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	res, err = queryNodesResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	err1 = json.Unmarshal(res, &nodeAccountResp)
	c.Assert(err1, IsNil)
	c.Assert(len(nodeAccountResp), Equals, 2)

	for _, node := range nodeAccountResp {
		if node.NodeAddress == nodeAccount.NodeAddress.String() {
			// First node has 25% of total bond, gets 25% of rewards
			c.Assert(node.CurrentAward, Equals, cosmos.NewUint(common.One*250).String())
			continue
		} else if node.NodeAddress == nodeAccount2.NodeAddress.String() {
			// Second node has 75% of total bond, gets 75% of rewards
			c.Assert(node.CurrentAward, Equals, cosmos.NewUint(common.One*750).String())
			continue
		}

		c.Fail()
	}

	// Check querier only returns nodes with bond
	nodeAccount2.Bond = cosmos.NewUint(0)
	c.Assert(keeper.SetNodeAccount(ctx, nodeAccount2), IsNil)

	queryNodesResp, err = queryServer.Nodes(ctx, &types.QueryNodesRequest{})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	res, err = queryNodesResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	err1 = json.Unmarshal(res, &out)
	c.Assert(err1, IsNil)
	c.Assert(len(out), Equals, 1)
}

func (s *QuerierSuite) TestQueryUpgradeProposals(c *C) {
	ctx, mgr := setupManagerForTest(c)
	queryServer := NewQueryServerImpl(mgr, s.kb)

	k := mgr.Keeper()

	// Add node accounts
	na1 := GetRandomValidatorNode(NodeActive)
	na1.Bond = cosmos.NewUint(100 * common.One)
	c.Assert(k.SetNodeAccount(ctx, na1), IsNil)
	na2 := GetRandomValidatorNode(NodeActive)
	na2.Bond = cosmos.NewUint(200 * common.One)
	c.Assert(k.SetNodeAccount(ctx, na2), IsNil)
	na3 := GetRandomValidatorNode(NodeActive)
	na3.Bond = cosmos.NewUint(300 * common.One)
	c.Assert(k.SetNodeAccount(ctx, na3), IsNil)
	na4 := GetRandomValidatorNode(NodeActive)
	na4.Bond = cosmos.NewUint(400 * common.One)
	c.Assert(k.SetNodeAccount(ctx, na4), IsNil)
	na5 := GetRandomValidatorNode(NodeActive)
	na5.Bond = cosmos.NewUint(500 * common.One)
	c.Assert(k.SetNodeAccount(ctx, na5), IsNil)
	na6 := GetRandomValidatorNode(NodeActive)
	na6.Bond = cosmos.NewUint(600 * common.One)
	c.Assert(k.SetNodeAccount(ctx, na6), IsNil)

	const (
		upgradeName = "1.2.3"
		upgradeInfo = "scheduled upgrade"
	)

	upgradeHeight := ctx.BlockHeight() + 100

	// propose upgrade
	c.Assert(k.ProposeUpgrade(ctx, upgradeName, types.UpgradeProposal{
		Height: upgradeHeight,
		Info:   upgradeInfo,
	}), IsNil)

	k.ApproveUpgrade(ctx, na1.NodeAddress, upgradeName)
	k.ApproveUpgrade(ctx, na2.NodeAddress, upgradeName)
	k.ApproveUpgrade(ctx, na3.NodeAddress, upgradeName)

	queryUpgradeProposalsResp, err := queryServer.UpgradeProposals(ctx, &types.QueryUpgradeProposalsRequest{})
	c.Assert(err, IsNil)
	res, err := queryUpgradeProposalsResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	var proposals []openapi.UpgradeProposal

	err = json.Unmarshal(res, &proposals)
	c.Assert(err, IsNil)

	c.Assert(len(proposals), Equals, 1)
	p := proposals[0]
	c.Assert(p.Name, Equals, upgradeName)
	c.Assert(p.Info, Equals, upgradeInfo)
	c.Assert(p.Height, Equals, upgradeHeight)

	queryUpgradeProposalResp, err := queryServer.UpgradeProposal(ctx, &types.QueryUpgradeProposalRequest{
		Name: upgradeName,
	})
	c.Assert(err, IsNil)
	res, err = queryUpgradeProposalResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	err = json.Unmarshal(res, &p)
	c.Assert(err, IsNil)

	c.Assert(p.Name, Equals, upgradeName)
	c.Assert(p.Info, Equals, upgradeInfo)
	c.Assert(p.Height, Equals, upgradeHeight)
	c.Assert(*p.Approved, Equals, false)
	c.Assert(*p.ValidatorsToQuorum, Equals, int64(1))
	c.Assert(*p.ApprovedPercent, Equals, "50.00")

	k.ApproveUpgrade(ctx, na4.NodeAddress, upgradeName)

	queryUpgradeProposalResp, err = queryServer.UpgradeProposal(ctx, &types.QueryUpgradeProposalRequest{
		Name: upgradeName,
	})
	c.Assert(err, IsNil)
	res, err = queryUpgradeProposalResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	err = json.Unmarshal(res, &p)
	c.Assert(err, IsNil)

	c.Assert(*p.Approved, Equals, true)
	c.Assert(*p.ValidatorsToQuorum, Equals, int64(0))
	c.Assert(*p.ApprovedPercent, Equals, "66.67")

	k.RejectUpgrade(ctx, na2.NodeAddress, upgradeName)

	queryUpgradeProposalResp, err = queryServer.UpgradeProposal(ctx, &types.QueryUpgradeProposalRequest{
		Name: upgradeName,
	})
	c.Assert(err, IsNil)
	res, err = queryUpgradeProposalResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	err = json.Unmarshal(res, &p)
	c.Assert(err, IsNil)

	c.Assert(*p.Approved, Equals, false)
	c.Assert(*p.ValidatorsToQuorum, Equals, int64(1))
	c.Assert(*p.ApprovedPercent, Equals, "50.00")

	var votes []openapi.UpgradeVote
	queryUpgradeVotesResp, err := queryServer.UpgradeVotes(ctx, &types.QueryUpgradeVotesRequest{
		Name: upgradeName,
	})
	c.Assert(err, IsNil)
	res, err = queryUpgradeVotesResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	err = json.Unmarshal(res, &votes)
	c.Assert(err, IsNil)
	c.Assert(len(votes), Equals, 4)

	foundVote := make(map[string]bool)
	for _, v := range votes {
		if _, ok := foundVote[v.NodeAddress]; ok {
			c.Log("duplicate vote", v.NodeAddress)
			c.Fail()
		}
		foundVote[v.NodeAddress] = true
		switch v.NodeAddress {
		case na1.NodeAddress.String():
			c.Assert(v.Vote, Equals, "approve")
		case na2.NodeAddress.String():
			c.Assert(v.Vote, Equals, "reject")
		case na3.NodeAddress.String():
			c.Assert(v.Vote, Equals, "approve")
		case na4.NodeAddress.String():
			c.Assert(v.Vote, Equals, "approve")
		case na5.NodeAddress.String():
			c.Assert(v.Vote, Equals, "approve")
		default:
			c.Log("unexpected voter address", v.NodeAddress)
			c.Fail()
		}
	}
}

func (s *QuerierSuite) TestQuerierRagnarokInProgress(c *C) {
	// test ragnarok
	queryRagnarokResp, err := s.queryServer.Ragnarok(s.ctx, &types.QueryRagnarokRequest{})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryRagnarokResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	c.Assert(result, NotNil)
	var ragnarok bool
	c.Assert(json.Unmarshal(result, &ragnarok), IsNil)
	c.Assert(ragnarok, Equals, false)
}

func (s *QuerierSuite) TestQueryLiquidityProviders(c *C) {
	// test liquidity providers
	queryLPsResp, err := s.queryServer.LiquidityProviders(s.ctx, &types.QueryLiquidityProvidersRequest{
		Asset: "ETH.ETH",
	})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryLPsResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	s.k.SetLiquidityProvider(s.ctx, LiquidityProvider{
		Asset:              common.ETHAsset,
		RuneAddress:        GetRandomETHAddress(),
		AssetAddress:       GetRandomETHAddress(),
		LastAddHeight:      1024,
		LastWithdrawHeight: 0,
		Units:              cosmos.NewUint(10),
	})
	queryLPsResp, err = s.queryServer.LiquidityProviders(s.ctx, &types.QueryLiquidityProvidersRequest{
		Asset: "ETH.ETH",
	})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err = queryLPsResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var lps LiquidityProviders
	c.Assert(json.Unmarshal(result, &lps), IsNil)
	c.Assert(lps, HasLen, 1)

	s.k.SetLiquidityProvider(s.ctx, LiquidityProvider{
		Asset:              common.ETHAsset.GetSyntheticAsset(),
		RuneAddress:        GetRandomETHAddress(),
		AssetAddress:       GetRandomRUNEAddress(),
		LastAddHeight:      1024,
		LastWithdrawHeight: 0,
		Units:              cosmos.NewUint(10),
	})

	// Query Savers from SaversPool
	querySaversResp, err := s.queryServer.Savers(s.ctx, &types.QuerySaversRequest{
		Asset: "ETH.ETH",
	})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err = querySaversResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	c.Assert(result, NotNil)
	var savers LiquidityProviders
	c.Assert(json.Unmarshal(result, &savers), IsNil)
	c.Assert(lps, HasLen, 1)
}

func (s *QuerierSuite) TestQueryTxInVoter(c *C) {
	tx := GetRandomTx()
	// test getTxInVoter
	queryTxVoterResp, err := s.queryServer.TxVoters(s.ctx, &types.QueryTxVotersRequest{
		TxId: tx.ID.String(),
	})
	c.Assert(err, NotNil)
	c.Assert(queryTxVoterResp, IsNil)

	observedTxInVote := NewObservedTxVoter(tx.ID, []ObservedTx{NewObservedTx(tx, s.ctx.BlockHeight(), GetRandomPubKey(), s.ctx.BlockHeight())})
	s.k.SetObservedTxInVoter(s.ctx, observedTxInVote)
	queryTxVoterResp, err = s.queryServer.TxVoters(s.ctx, &types.QueryTxVotersRequest{
		TxId: tx.ID.String(),
	})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryTxVoterResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	c.Assert(result, NotNil)
	var voter openapi.TxDetailsResponse
	c.Assert(json.Unmarshal(result, &voter), IsNil)

	// common.Tx Valid cannot be used for openapi.Tx, so checking some criteria individually.
	c.Assert(voter.TxId == nil, Equals, false)
	c.Assert(len(voter.Txs) == 1, Equals, true)
	c.Assert(voter.Txs[0].ExternalObservedHeight == nil, Equals, false)
	c.Assert(*voter.Txs[0].ExternalObservedHeight <= 0, Equals, false)
	c.Assert(voter.Txs[0].ObservedPubKey == nil, Equals, false)
	c.Assert(voter.Txs[0].ExternalConfirmationDelayHeight == nil, Equals, false)
	c.Assert(*voter.Txs[0].ExternalConfirmationDelayHeight <= 0, Equals, false)
	c.Assert(voter.Txs[0].Tx.Id == nil, Equals, false)
	c.Assert(voter.Txs[0].Tx.FromAddress == nil, Equals, false)
	c.Assert(voter.Txs[0].Tx.ToAddress == nil, Equals, false)
	c.Assert(voter.Txs[0].Tx.Chain == nil, Equals, false)
	c.Assert(len(voter.Txs[0].Tx.Coins) == 0, Equals, false)
}

func (s *QuerierSuite) TestQueryTxStages(c *C) {
	tx := GetRandomTx()
	// test getTxInVoter
	queryTxStagesResp, err := s.queryServer.TxStages(s.ctx, &types.QueryTxStagesRequest{
		TxId: tx.ID.String(),
	})
	c.Assert(err, IsNil) // Expecting no error for an unobserved hash.

	// marshal output so we can verify it unmarshals as expected
	result, err := queryTxStagesResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil) // Expecting a not-started Observation stage.
	c.Assert(err, IsNil)     // Expecting no error for an unobserved hash.
	observedTxInVote := NewObservedTxVoter(tx.ID, []ObservedTx{NewObservedTx(tx, s.ctx.BlockHeight(), GetRandomPubKey(), s.ctx.BlockHeight())})
	s.k.SetObservedTxInVoter(s.ctx, observedTxInVote)
	queryTxStagesResp, err = s.queryServer.TxStages(s.ctx, &types.QueryTxStagesRequest{
		TxId: tx.ID.String(),
	})
	c.Assert(err, IsNil) // Expecting no error for an unobserved hash.

	// marshal output so we can verify it unmarshals as expected
	result, err = queryTxStagesResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil) // Expecting a not-started Observation stage.
	c.Assert(err, IsNil)     // Expecting no error for an unobserved hash.

	// Verify conformance to openapi spec
	var openapiTxStagesResp openapi.TxStagesResponse
	err = json.Unmarshal(result, &openapiTxStagesResp)
	c.Assert(err, IsNil)

	c.Assert(*openapiTxStagesResp.InboundObserved.Started, Equals, queryTxStagesResp.InboundObserved.Started)
}

func (s *QuerierSuite) TestQueryTxStatus(c *C) {
	tx := GetRandomTx()
	// test getTxInVoter
	queryTxStatusResp, err := s.queryServer.TxStatus(s.ctx, &types.QueryTxStatusRequest{
		TxId: tx.ID.String(),
	})
	c.Assert(err, IsNil) // Expecting no error for an unobserved hash.

	// marshal output so we can verify it unmarshals as expected
	result, err := queryTxStatusResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil) // Expecting a not-started Observation stage.
	c.Assert(err, IsNil)     // Expecting no error for an unobserved hash.
	observedTxInVote := NewObservedTxVoter(tx.ID, []ObservedTx{NewObservedTx(tx, s.ctx.BlockHeight(), GetRandomPubKey(), s.ctx.BlockHeight())})
	s.k.SetObservedTxInVoter(s.ctx, observedTxInVote)
	queryTxStatusResp, err = s.queryServer.TxStatus(s.ctx, &types.QueryTxStatusRequest{
		TxId: tx.ID.String(),
	})
	c.Assert(err, IsNil) // Expecting no error for an unobserved hash.

	// marshal output so we can verify it unmarshals as expected
	result, err = queryTxStatusResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	c.Assert(result, NotNil)

	// Verify conformance to openapi spec
	var openapiTxStatusresp openapi.TxStatusResponse
	err = json.Unmarshal(result, &openapiTxStatusresp)
	c.Assert(err, IsNil)

	c.Assert(*openapiTxStatusresp.Tx.Id, Equals, queryTxStatusResp.Tx.ID.String())
	c.Assert(*openapiTxStatusresp.Tx.Chain, Equals, queryTxStatusResp.Tx.Chain.String())
	c.Assert(*openapiTxStatusresp.Tx.FromAddress, Equals, queryTxStatusResp.Tx.FromAddress.String())
	c.Assert(*openapiTxStatusresp.Tx.ToAddress, Equals, queryTxStatusResp.Tx.ToAddress.String())
	c.Assert(openapiTxStatusresp.Tx.Coins[0].Asset, Equals, queryTxStatusResp.Tx.Coins[0].Asset.String())
	c.Assert(openapiTxStatusresp.Tx.Coins[0].Amount, Equals, queryTxStatusResp.Tx.Coins[0].Amount.String())
	c.Assert(openapiTxStatusresp.Tx.Gas[0].Asset, Equals, queryTxStatusResp.Tx.Gas[0].Asset.String())
	c.Assert(openapiTxStatusresp.Tx.Gas[0].Amount, Equals, queryTxStatusResp.Tx.Gas[0].Amount.String())
	c.Assert(openapiTxStatusresp.OutTxs, IsNil)
	c.Assert(*openapiTxStatusresp.Stages.InboundObserved.Started, Equals, queryTxStatusResp.Stages.InboundObserved.Started)
}

func (s *QuerierSuite) TestQueryTx(c *C) {
	tx := GetRandomTx()
	// test get tx in
	queryTxResp, err := s.queryServer.Tx(s.ctx, &types.QueryTxRequest{
		TxId: tx.ID.String(),
	})
	c.Assert(err, NotNil)
	c.Assert(queryTxResp, IsNil)

	nodeAccount := GetRandomValidatorNode(NodeActive)
	c.Assert(s.k.SetNodeAccount(s.ctx, nodeAccount), IsNil)
	voter, err := s.k.GetObservedTxInVoter(s.ctx, tx.ID)
	c.Assert(err, IsNil)
	voter.Add(NewObservedTx(tx, s.ctx.BlockHeight(), nodeAccount.PubKeySet.Secp256k1, s.ctx.BlockHeight()), nodeAccount.NodeAddress)
	s.k.SetObservedTxInVoter(s.ctx, voter)
	queryTxResp, err = s.queryServer.Tx(s.ctx, &types.QueryTxRequest{
		TxId: tx.ID.String(),
	})
	c.Assert(err, IsNil)

	result, err := queryTxResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	var newTx struct {
		openapi.ObservedTx `json:"observed_tx"`
		KeysignMetrics     types.TssKeysignMetric `json:"keysign_metric,omitempty"`
	}
	c.Assert(json.Unmarshal(result, &newTx), IsNil)

	// common.Tx Valid cannot be used for openapi.Tx, so checking some criteria individually.
	c.Assert(newTx.ExternalObservedHeight == nil, Equals, false)
	c.Assert(*newTx.ExternalObservedHeight <= 0, Equals, false)
	c.Assert(newTx.ObservedPubKey == nil, Equals, false)
	c.Assert(newTx.ExternalConfirmationDelayHeight == nil, Equals, false)
	c.Assert(*newTx.ExternalConfirmationDelayHeight <= 0, Equals, false)
	c.Assert(newTx.Tx.Id == nil, Equals, false)
	c.Assert(newTx.Tx.FromAddress == nil, Equals, false)
	c.Assert(newTx.Tx.ToAddress == nil, Equals, false)
	c.Assert(newTx.Tx.Chain == nil, Equals, false)
	c.Assert(len(newTx.Tx.Coins) == 0, Equals, false)
}

func (s *QuerierSuite) TestQueryKeyGen(c *C) {
	queryKeygensPubkeyResp, err := s.queryServer.Keygen(s.ctx, &types.QueryKeygenRequest{
		Height: "whatever",
	})
	c.Assert(queryKeygensPubkeyResp, IsNil)
	c.Assert(err, NotNil)

	queryKeygensPubkeyResp, err = s.queryServer.Keygen(s.ctx, &types.QueryKeygenRequest{
		Height: "10000",
	})
	c.Assert(queryKeygensPubkeyResp, IsNil)
	c.Assert(err, NotNil)

	queryKeygensPubkeyResp, err = s.queryServer.Keygen(s.ctx, &types.QueryKeygenRequest{
		Height: strconv.FormatInt(s.ctx.BlockHeight(), 10),
	})
	c.Assert(queryKeygensPubkeyResp, NotNil)
	c.Assert(err, IsNil)

	queryKeygensPubkeyResp, err = s.queryServer.Keygen(s.ctx, &types.QueryKeygenRequest{
		Height: strconv.FormatInt(s.ctx.BlockHeight(), 10),
		PubKey: GetRandomPubKey().String(),
	})
	c.Assert(queryKeygensPubkeyResp, NotNil)
	c.Assert(err, IsNil)

	result, err := queryKeygensPubkeyResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	// Verify conformance to openapi spec
	var openapiKeygensPubkeyResp openapi.KeygenResponse
	err = json.Unmarshal(result, &openapiKeygensPubkeyResp)
	c.Assert(err, IsNil)

	c.Assert(*openapiKeygensPubkeyResp.KeygenBlock.Height, Equals, queryKeygensPubkeyResp.KeygenBlock.Height)
	c.Assert(openapiKeygensPubkeyResp.KeygenBlock.Keygens, IsNil)
	c.Assert(openapiKeygensPubkeyResp.Signature, Equals, queryKeygensPubkeyResp.Signature)
}

func (s *QuerierSuite) TestQueryQueue(c *C) {
	queryQueueResp, err := s.queryServer.Queue(s.ctx, &types.QueryQueueRequest{})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryQueueResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var q openapi.QueueResponse
	c.Assert(json.Unmarshal(result, &q), IsNil)

	// Verify conformance to openapi spec
	c.Assert(q.ScheduledOutboundClout, Equals, queryQueueResp.ScheduledOutboundClout)
	c.Assert(q.ScheduledOutboundValue, Equals, queryQueueResp.ScheduledOutboundValue)
}

func (s *QuerierSuite) TestQueryHeights(c *C) {
	queryChainsLastBlockResp, err := s.queryServer.ChainsLastBlock(s.ctx, &types.QueryChainsLastBlockRequest{
		Chain: strconv.FormatInt(s.ctx.BlockHeight(), 10),
	})
	c.Assert(queryChainsLastBlockResp, IsNil)
	c.Assert(err, NotNil)

	queryLastBlocksResp, err := s.queryServer.LastBlocks(s.ctx, &types.QueryLastBlocksRequest{})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryLastBlocksResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var q []openapi.LastBlock
	c.Assert(json.Unmarshal(result, &q), IsNil)

	queryChainsLastBlockResp, err = s.queryServer.ChainsLastBlock(s.ctx, &types.QueryChainsLastBlockRequest{
		Chain: "BTC",
	})
	c.Assert(err, IsNil)

	result, err = queryChainsLastBlockResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	c.Assert(json.Unmarshal(result, &q), IsNil)

	// Verify conformance to openapi spec
	c.Assert(q[0].Chain, Equals, queryChainsLastBlockResp.LastBlocks[0].Chain)
	c.Assert(q[0].Thorchain, Equals, queryChainsLastBlockResp.LastBlocks[0].Thorchain)
}

func (s *QuerierSuite) TestQueryConstantValues(c *C) {
	queryConstantValResp, err := s.queryServer.ConstantValues(s.ctx, &types.QueryConstantValuesRequest{})
	c.Assert(queryConstantValResp, NotNil)
	c.Assert(err, IsNil)

	_, err = queryConstantValResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	// Note: openapi conformance isn't followed here nor legacy versions
	// openapi's ConstantsResponse
	//   int_64_values are map[string]string while map[string]int64 is actually returned
	//   bool_values are map[string]string while map[string]bool is actually returned
}

func (s *QuerierSuite) TestQueryMimir(c *C) {
	s.k.SetMimir(s.ctx, "hello", 111)
	queryMimirResp, err := s.queryServer.MimirValues(s.ctx, &types.QueryMimirValuesRequest{})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryMimirResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	c.Assert(result, NotNil)
	var m map[string]int64
	c.Assert(json.Unmarshal(result, &m), IsNil)
	c.Assert(m, HasLen, 1)
	c.Assert(m["HELLO"], Equals, int64(111))
}

func (s *QuerierSuite) TestQueryBan(c *C) {
	queryBanResp, err := s.queryServer.Ban(s.ctx, &types.QueryBanRequest{})
	c.Assert(queryBanResp, IsNil)
	c.Assert(err, NotNil)

	queryBanResp, err = s.queryServer.Ban(s.ctx, &types.QueryBanRequest{
		Address: "Whatever",
	})
	c.Assert(queryBanResp, IsNil)
	c.Assert(err, NotNil)

	queryBanResp, err = s.queryServer.Ban(s.ctx, &types.QueryBanRequest{
		Address: GetRandomBech32Addr().String(),
	})
	c.Assert(queryBanResp, NotNil)
	c.Assert(err, IsNil)

	result, err := queryBanResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)

	var openapiBanResp openapi.BanResponse
	err = json.Unmarshal(result, &openapiBanResp)
	c.Assert(err, IsNil)

	// Verify conformance to openapi spec
	c.Assert(*openapiBanResp.NodeAddress, Equals, queryBanResp.NodeAddress.String())
}

func (s *QuerierSuite) TestQueryNodeAccount(c *C) {
	queryNodeAccount, err := s.queryServer.Node(s.ctx, &types.QueryNodeRequest{})
	c.Assert(queryNodeAccount, IsNil)
	c.Assert(err, NotNil)

	queryNodeAccount, err = s.queryServer.Node(s.ctx, &types.QueryNodeRequest{
		Address: "Whatever",
	})
	c.Assert(queryNodeAccount, IsNil)
	c.Assert(err, NotNil)

	na := GetRandomValidatorNode(NodeActive)
	c.Assert(s.k.SetNodeAccount(s.ctx, na), IsNil)
	vault := GetRandomVault()
	vault.Status = ActiveVault
	vault.BlockHeight = 1
	c.Assert(s.k.SetVault(s.ctx, vault), IsNil)
	queryNodeAccount, err = s.queryServer.Node(s.ctx, &types.QueryNodeRequest{
		Address: na.NodeAddress.String(),
	})
	c.Assert(queryNodeAccount, NotNil)
	c.Assert(err, IsNil)

	result, err := queryNodeAccount.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var r openapi.Node
	c.Assert(json.Unmarshal(result, &r), IsNil)

	// Check bond-weighted rewards estimation works

	// Add another node with 75% of the bond
	nodeAccount2 := GetRandomValidatorNode(NodeActive)
	nodeAccount2.Bond = cosmos.NewUint(common.One * 3000)
	c.Assert(s.k.SetNodeAccount(s.ctx, nodeAccount2), IsNil)

	// Add bond rewards + set min bond for bond-weighted system
	network, _ := s.k.GetNetwork(s.ctx)
	network.BondRewardRune = cosmos.NewUint(common.One * 1000)
	c.Assert(s.k.SetNetwork(s.ctx, network), IsNil)
	s.k.SetMimir(s.ctx, "MinimumBondInRune", common.One*1000)

	// Get first node
	queryNodeAccount, err = s.queryServer.Node(s.ctx, &types.QueryNodeRequest{
		Address: na.NodeAddress.String(),
	})
	c.Assert(queryNodeAccount, NotNil)
	c.Assert(err, IsNil)

	result, err = queryNodeAccount.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var r2 openapi.Node
	c.Assert(json.Unmarshal(result, &r2), IsNil)

	// First node has 25% of bond, should have 25% of the rewards
	c.Assert(r2.TotalBond, Equals, cosmos.NewUint(common.One*1000).String())
	c.Assert(r2.CurrentAward, Equals, cosmos.NewUint(common.One*250).String())

	// Get second node
	queryNodeAccount, err = s.queryServer.Node(s.ctx, &types.QueryNodeRequest{
		Address: nodeAccount2.NodeAddress.String(),
	})
	c.Assert(queryNodeAccount, NotNil)
	c.Assert(err, IsNil)

	result, err = queryNodeAccount.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var r3 openapi.Node
	c.Assert(json.Unmarshal(result, &r3), IsNil)

	// Second node has 75% of bond, should have 75% of the rewards
	c.Assert(r3.TotalBond, Equals, cosmos.NewUint(common.One*3000).String())
	c.Assert(r3.CurrentAward, Equals, cosmos.NewUint(common.One*750).String())
}

func (s *QuerierSuite) TestQueryPoolAddresses(c *C) {
	ctx, mgr := setupManagerForTest(c)

	pubKey := GetRandomPubKey()
	asgard := NewVault(ctx.BlockHeight()-1, ActiveVault, AsgardVault, pubKey, common.Chains{common.ETHChain}.Strings(), nil)
	c.Assert(mgr.Keeper().SetVault(ctx, asgard), IsNil)

	queryServer := NewQueryServerImpl(mgr, s.kb)
	queryInboundAddrResp, err := queryServer.InboundAddresses(ctx, &types.QueryInboundAddressesRequest{})
	c.Assert(err, IsNil)
	result, err := queryInboundAddrResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)

	resp := []struct {
		Chain   common.Chain   `json:"chain"`
		PubKey  common.PubKey  `json:"pub_key"`
		Address common.Address `json:"address"`
		Halted  bool           `json:"halted"`
	}{}

	c.Assert(json.Unmarshal(result, &resp), IsNil)
	c.Assert(len(resp), Equals, 1)
	c.Assert(resp[0].Chain, Equals, common.ETHChain)
	c.Assert(resp[0].PubKey, Equals, pubKey)
}

func (s *QuerierSuite) TestQueryKeysignArrayPubKey(c *C) {
	na := GetRandomValidatorNode(NodeActive)
	c.Assert(s.k.SetNodeAccount(s.ctx, na), IsNil)
	queryKeysignPubkeyResp, err := s.queryServer.KeysignPubkey(s.ctx, &types.QueryKeysignPubkeyRequest{})
	c.Assert(queryKeysignPubkeyResp, IsNil)
	c.Assert(err, NotNil)

	queryKeysignPubkeyResp, err = s.queryServer.KeysignPubkey(s.ctx, &types.QueryKeysignPubkeyRequest{
		Height: "asdf",
	})
	c.Assert(queryKeysignPubkeyResp, IsNil)
	c.Assert(err, NotNil)

	queryKeysignPubkeyResp, err = s.queryServer.KeysignPubkey(s.ctx, &types.QueryKeysignPubkeyRequest{
		Height: strconv.FormatInt(s.ctx.BlockHeight(), 10),
	})
	c.Assert(queryKeysignPubkeyResp, NotNil)
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryKeysignPubkeyResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	c.Assert(result, NotNil)
	var r openapi.KeysignResponse
	c.Assert(json.Unmarshal(result, &r), IsNil)

	// Verify conformance to openapi spec
	c.Assert(*r.Keysign.Height, Equals, queryKeysignPubkeyResp.Keysign.Height)
	c.Assert(len(r.Keysign.TxArray), Equals, 0)
	c.Assert(r.Signature, Equals, queryKeysignPubkeyResp.Signature)
}

func (s *QuerierSuite) TestQueryNetwork(c *C) {
	queryNetworkResp, err := s.queryServer.Network(s.ctx, &types.QueryNetworkRequest{})
	c.Assert(err, IsNil)

	// QueryNetworkResponse does not require JSONPBMarshaler implementation
	result, err := json.Marshal(queryNetworkResp)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var r Network // unsure why we were unmarshaling to this type, but leaving in
	c.Assert(json.Unmarshal(result, &r), IsNil)

	// Verify conformance to openapi spec
	var openapiNetworkResponse openapi.NetworkResponse
	c.Assert(json.Unmarshal(result, &openapiNetworkResponse), IsNil)

	c.Assert(openapiNetworkResponse.BondRewardRune, Equals, queryNetworkResp.BondRewardRune)
	c.Assert(openapiNetworkResponse.EffectiveSecurityBond, Equals, queryNetworkResp.EffectiveSecurityBond)
	c.Assert(openapiNetworkResponse.GasSpentRune, Equals, queryNetworkResp.GasSpentRune)
	c.Assert(openapiNetworkResponse.GasWithheldRune, Equals, queryNetworkResp.GasWithheldRune)
	c.Assert(openapiNetworkResponse.NativeOutboundFeeRune, Equals, queryNetworkResp.NativeOutboundFeeRune)
	c.Assert(openapiNetworkResponse.NativeTxFeeRune, Equals, queryNetworkResp.NativeTxFeeRune)
	c.Assert(*openapiNetworkResponse.OutboundFeeMultiplier, Equals, queryNetworkResp.OutboundFeeMultiplier)
	c.Assert(openapiNetworkResponse.RunePriceInTor, Equals, queryNetworkResp.RunePriceInTor)
	c.Assert(openapiNetworkResponse.TnsFeePerBlockRune, Equals, queryNetworkResp.TnsFeePerBlockRune)
	c.Assert(openapiNetworkResponse.TnsRegisterFeeRune, Equals, queryNetworkResp.TnsRegisterFeeRune)
	c.Assert(openapiNetworkResponse.TorPriceInRune, Equals, queryNetworkResp.TorPriceInRune)
	c.Assert(openapiNetworkResponse.TotalBondUnits, Equals, queryNetworkResp.TotalBondUnits)
	c.Assert(openapiNetworkResponse.TotalReserve, Equals, queryNetworkResp.TotalReserve)
	c.Assert(openapiNetworkResponse.VaultsMigrating, Equals, queryNetworkResp.VaultsMigrating)
}

func (s *QuerierSuite) TestQueryAsgardVault(c *C) {
	c.Assert(s.k.SetVault(s.ctx, GetRandomVault()), IsNil)
	queryAsgardVaultResp, err := s.queryServer.AsgardVaults(s.ctx, &types.QueryAsgardVaultsRequest{})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryAsgardVaultResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var r Vaults
	c.Assert(json.Unmarshal(result, &r), IsNil)

	// Verify conformance to openapi spec
	var openapiVaults []openapi.Vault
	err = json.Unmarshal(result, &openapiVaults)
	c.Assert(err, IsNil)

	c.Assert(len(openapiVaults[0].Addresses), Equals, len(queryAsgardVaultResp.AsgardVaults[0].Addresses))
	c.Assert(*openapiVaults[0].BlockHeight, Equals, queryAsgardVaultResp.AsgardVaults[0].BlockHeight)
	c.Assert(*openapiVaults[0].PubKey, Equals, queryAsgardVaultResp.AsgardVaults[0].PubKey)
	c.Assert(*openapiVaults[0].Type, Equals, queryAsgardVaultResp.AsgardVaults[0].Type)
	c.Assert(openapiVaults[0].Status, Equals, queryAsgardVaultResp.AsgardVaults[0].Status)
	c.Assert(*openapiVaults[0].StatusSince, Equals, queryAsgardVaultResp.AsgardVaults[0].StatusSince)
	c.Assert(len(openapiVaults[0].Chains), Equals, len(queryAsgardVaultResp.AsgardVaults[0].Chains))
}

func (s *QuerierSuite) TestQueryVaultPubKeys(c *C) {
	node := GetRandomValidatorNode(NodeActive)
	c.Assert(s.k.SetNodeAccount(s.ctx, node), IsNil)
	vault := GetRandomVault()
	vault.PubKey = node.PubKeySet.Secp256k1
	vault.Type = AsgardVault
	vault.AddFunds(common.Coins{
		common.NewCoin(common.ETHAsset, cosmos.NewUint(common.One*100)),
	})
	vault.Routers = []types.ChainContract{
		{
			Chain:  "ETH",
			Router: "0xE65e9d372F8cAcc7b6dfcd4af6507851Ed31bb44",
		},
	}
	c.Assert(s.k.SetVault(s.ctx, vault), IsNil)
	vault1 := GetRandomVault()
	vault1.Routers = vault.Routers
	c.Assert(s.k.SetVault(s.ctx, vault1), IsNil)
	queryVaultPubkeys, err := s.queryServer.VaultsPubkeys(s.ctx, &types.QueryVaultsPubkeysRequest{})
	c.Assert(err, IsNil)

	// QueryVaultsPubkeysResponse does not require JSONPBMarshaler implementation
	result, err := json.Marshal(queryVaultPubkeys)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var r openapi.VaultPubkeysResponse
	c.Assert(json.Unmarshal(result, &r), IsNil)

	// Verify conformance to openapi spec
	c.Assert(r.Asgard[0].PubKey, Equals, queryVaultPubkeys.Asgard[0].PubKey)
	c.Assert(*r.Asgard[0].Routers[0].Chain, Equals, queryVaultPubkeys.Asgard[0].Routers[0].Chain)
	c.Assert(*r.Asgard[0].Routers[0].Router, Equals, queryVaultPubkeys.Asgard[0].Routers[0].Router)
}

func (s *QuerierSuite) TestQueryBalanceModule(c *C) {
	c.Assert(s.k.SetVault(s.ctx, GetRandomVault()), IsNil)
	queryBalanceModulesResp, err := s.queryServer.BalanceModule(s.ctx, &types.QueryBalanceModuleRequest{
		Name: "asgard",
	})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryBalanceModulesResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var r struct {
		Name    string            `json:"name"`
		Address cosmos.AccAddress `json:"address"`
		Coins   types2.Coins      `json:"coins"`
	}
	c.Assert(json.Unmarshal(result, &r), IsNil)

	// Verify conformance to legacy output
	c.Assert(r.Address.String(), Equals, queryBalanceModulesResp.Address.String())
	c.Assert(r.Coins[0].Amount.String(), Equals, queryBalanceModulesResp.Coins[0].Amount.String())
	c.Assert(r.Coins[0].Denom, Equals, queryBalanceModulesResp.Coins[0].Denom)
	c.Assert(r.Name, Equals, queryBalanceModulesResp.Name)
}

func (s *QuerierSuite) TestQueryVault(c *C) {
	vault := GetRandomVault()

	queryVaultResp, err := s.queryServer.Vault(s.ctx, &types.QueryVaultRequest{
		PubKey: "ETH",
	})
	c.Assert(queryVaultResp, IsNil)
	c.Assert(err, NotNil)

	c.Assert(s.k.SetVault(s.ctx, vault), IsNil)
	queryVaultResp, err = s.queryServer.Vault(s.ctx, &types.QueryVaultRequest{
		PubKey: vault.PubKey.String(),
	})
	c.Assert(err, IsNil)

	// marshal output so we can verify it unmarshals as expected
	result, err := queryVaultResp.MarshalJSONPB(nil)
	c.Assert(err, IsNil)
	c.Assert(result, NotNil)
	var returnVault Vault
	c.Assert(json.Unmarshal(result, &returnVault), IsNil)
	c.Assert(vault.PubKey.Equals(returnVault.PubKey), Equals, true)
	c.Assert(vault.Type, Equals, returnVault.Type)
	c.Assert(vault.Status, Equals, returnVault.Status)
	c.Assert(vault.BlockHeight, Equals, returnVault.BlockHeight)
}

func (s *QuerierSuite) TestQueryVersion(c *C) {
	queryVersionResp, err := s.queryServer.Version(s.ctx, &types.QueryVersionRequest{})
	c.Assert(err, IsNil)

	result, err := queryVersionResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	var r openapi.VersionResponse
	c.Assert(json.Unmarshal(result, &r), IsNil)

	verComputed := s.k.GetLowestActiveVersion(s.ctx)
	c.Assert(r.Current, Equals, verComputed.String(),
		Commentf("query should return same version as computed"))

	// override the version computed in BeginBlock
	s.k.SetVersionWithCtx(s.ctx, semver.MustParse("4.5.6"))

	queryVersionResp, err = s.queryServer.Version(s.ctx, &types.QueryVersionRequest{})
	c.Assert(err, IsNil)

	result, err = queryVersionResp.MarshalJSONPB(nil)
	c.Assert(result, NotNil)
	c.Assert(err, IsNil)
	c.Assert(json.Unmarshal(result, &r), IsNil)
	c.Assert(r.Current, Equals, "4.5.6",
		Commentf("query should use stored version"))
}

func (s *QuerierSuite) TestPeerIDFromPubKey(c *C) {
	// Success example, secp256k1 pubkey from Mocknet node tthor1jgnk2mg88m57csrmrlrd6c3qe4lag3e33y2f3k
	var mocknetPubKey common.PubKey = "tthorpub1addwnpepqt8tnluxnk3y5quyq952klgqnlmz2vmaynm40fp592s0um7ucvjh5lc2l2z"
	c.Assert(getPeerIDFromPubKey(mocknetPubKey), Equals, "16Uiu2HAm9LeTqHJWSa67eHNZzSz3yKb64dbj7A4V1Ckv9hXyDkQR")

	// Failure example.
	expectedErrorString := "fail to parse account pub key(nonsense): decoding bech32 failed: invalid separator index -1"
	c.Assert(getPeerIDFromPubKey("nonsense"), Equals, expectedErrorString)
}

func (s *QuerierSuite) TestQuerySecuredAsset(c *C) {
	owner := GetRandomBech32Addr()
	addr := GetRandomBTCAddress()

	_, err := s.mgr.SecuredAssetManager().Deposit(s.ctx, common.BTCAsset, cosmos.NewUint(1000), owner, addr, common.BlankTxID)
	c.Assert(err, IsNil)

	result, err := s.queryServer.SecuredAsset(s.ctx, &types.QuerySecuredAssetRequest{
		Asset: "btc-btc",
	})

	c.Assert(err, IsNil)
	c.Assert(result, NotNil)
	c.Assert(result.Asset, Equals, "BTC-BTC")
	c.Assert(result.Depth, Equals, "1000")
	c.Assert(result.Supply, Equals, "1000")
}

func (s *QuerierSuite) TestQuerySecuredAssets(c *C) {
	owner := GetRandomBech32Addr()
	addr := GetRandomBTCAddress()

	_, err := s.mgr.SecuredAssetManager().Deposit(s.ctx, common.BTCAsset, cosmos.NewUint(1000), owner, addr, common.BlankTxID)
	c.Assert(err, IsNil)

	_, err = s.mgr.SecuredAssetManager().Deposit(s.ctx, common.ETHAsset, cosmos.NewUint(2000), owner, addr, common.BlankTxID)
	c.Assert(err, IsNil)

	result, err := s.queryServer.SecuredAssets(s.ctx, &types.QuerySecuredAssetsRequest{})

	c.Assert(err, IsNil)
	c.Assert(result, NotNil)
	c.Assert(len(result.Assets), Equals, 2)
	asset := result.Assets[0]
	c.Assert(asset.Asset, Equals, "BTC-BTC")
	c.Assert(asset.Depth, Equals, "1000")
	c.Assert(asset.Supply, Equals, "1000")

	asset = result.Assets[1]
	c.Assert(asset.Asset, Equals, "ETH-ETH")
	c.Assert(asset.Depth, Equals, "2000")
	c.Assert(asset.Supply, Equals, "2000")
}
